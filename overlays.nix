{ inputs, ...}: {
  nixpkgs.overlays = [
    inputs.nur.overlay # Nix User Repository
  ];
}
