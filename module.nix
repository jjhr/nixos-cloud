{ config, options, lib, pkgs, inputs, ... }:

let
  cfg = config.homelab;
  inherit (lib) mkEnableOption mkOption mkIf mkForce mkMerge mkDefault types mapAttrs optionalAttrs;
in
{

  imports = [
    inputs.sops-nix.nixosModules.default

    ./lib.nix
    ./overlays.nix

    ./boot.nix
    ./cloud
    ./containers.nix
    ./desktop
    ./ephemeral
    ./hardware
    ./home
    ./hosts.nix

  ];

  options.homelab = {

    inherit (options.networking) hostId hostName;
    machineId = mkOption { type = types.str; };
    secrets = options.sops.defaultSopsFile;

    useNetworkmanager = mkEnableOption "networkmanager";

    useCustomKbd = mkEnableOption "us-intl Alt GR without caps lock"
      // { default = true; };

    managed = mkEnableOption "managing this device via ssh";

    adminKeys = mkOption {
      description = "SSH Keys which are authorized on this device";
      type = types.listOf types.str;
    };
    tailnet.ip = mkOption {
      type = types.str;
      description = "Tailnet IP";
    };

  };

  config = mkMerge [

    {

      environment.etc."machine-id".text = cfg.machineId;
      networking = { inherit (cfg) hostId hostName; };

      time.timeZone = "Europe/Amsterdam";
      users.mutableUsers = false;

      nix = {
        package = pkgs.nixVersions.latest;
        extraOptions = ''
          experimental-features = nix-command flakes
        '';

        settings.auto-optimise-store = true;
        gc = {
          automatic = true;
          dates = "weekly";
          options = "--delete-older-than 30d";
        };
        settings.substituters = [ "https://nix-community.cachix.org" ];
        settings.trusted-public-keys = [ "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" ];

        # add all flake inputs to registry
        registry = mapAttrs (_: flake: { inherit flake; })
          ({ inherit (inputs) nixpkgs; }
            // optionalAttrs (inputs ? nur) { inherit (inputs) nur; }
            // optionalAttrs (inputs ? unstable) { inherit (inputs) unstable; }
          );

      };

      # Keyboard

      console.earlySetup = true;
      console.useXkbConfig = true;
      services.xserver = mkIf cfg.useCustomKbd {
        xkb = {
          layout = "us";
          variant = "altgr-intl";
          options = "caps:escape, eurosign:e";
        };
        autoRepeatDelay = 200;
        autoRepeatInterval = 20;
      };

      # Security / Secrets

      sops = {
        defaultSopsFile = cfg.secrets;
        age.sshKeyPaths = [ "/backup/etc/ssh/ssh_host_ed25519_key" ];
      };

      security = {
        sudo.enable = false;
        doas = {
          enable = true;
          extraRules = [{
            groups = [ "wheel" ];
            keepEnv = true;
            persist = true; # Don't ask for password for a while
          }];
        };
        apparmor.enable = mkDefault true;
      };
      programs.bash.interactiveShellInit = ''
        complete -F _command doas
      '';
      hardware = {
        enableRedistributableFirmware = mkDefault true;
        cpu.intel.updateMicrocode = mkDefault true;
        cpu.amd.updateMicrocode = mkDefault true;
      };

      # System packages

      # vim with nix plugin
      programs.vim = {
        defaultEditor = true;
        package = pkgs.vim_configurable.customize {
          name = "vim";
          vimrcConfig.packages.myplugins =
            with pkgs.vimPlugins; { start = [ vim-nix ]; };
          vimrcConfig.customRC = ''
            set tabstop=4 softtabstop=4 shiftwidth=4 expandtab
            syntax on
            set number relativenumber
          '';
        };
      };

      environment = {
        defaultPackages = mkForce [ ];
        systemPackages = with pkgs; [
          nixpkgs-fmt
          git
          htop
          gnumake
          rsync
          tmux
          lf
          fzf
          jq
          bat
          file
          lsof
          coreutils
          usbutils
          inetutils
        ];

        shellAliases = {
          dst = "doas systemctl status";
          dsr = "doas systemctl restart";
          dsa = "doas systemctl start";
          dsp = "doas systemctl stop";
          ju = "doas journalctl -feu";

          gis = "git status";
          gip = "git pull";
        };
      };

      services.openssh = {
        enable = true;
        allowSFTP = true;
        settings = {
          PasswordAuthentication = false;
          KbdInteractiveAuthentication = false;
          PermitRootLogin = "no";
          AllowTcpForwarding = true;
          X11Forwarding = false;
          AllowAgentForwarding = false;
          AllowStreamLocalForwarding = false;
          AuthenticationMethods = "publickey";
        };
      };

    }

    {
      networking =
        if !cfg.useNetworkmanager
        then { useDHCP = true; }
        else {
          useDHCP = false;
          wireless.enable = false;
          networkmanager.enable = true;
        };
    }

    (mkIf cfg.managed {

      # import unsigned store paths
      nix.settings.trusted-users = [ "atman" ];

      users.users.atman = {
        isNormalUser = true;
        extraGroups = lib.mkForce [ "wheel" ];
        openssh.authorizedKeys.keys = cfg.adminKeys;
        home = "/tmp/atman";
      };

      security.doas.extraConfig = ''
        permit nopass atman
      '';

      system.build.deploy =
        let
          inherit (cfg) hostName;
          inherit (cfg.tailnet) ip;
        in
        {
          nodes."${hostName}" = {
            sudo = "doas -u";
            sshUser = "atman";
            user = "root";
            hostname = ip;
            profiles.system.path = inputs.deploy-rs.lib."${pkgs.system}".activate.nixos { inherit config; };
          };
        };
    })

  ];

}
