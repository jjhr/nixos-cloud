{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nur.url = "github:nix-community/NUR";
    home-manager = { url = "github:nix-community/home-manager/release-24.11"; inputs.nixpkgs.follows = "nixpkgs"; };

    flake-utils.url = "github:numtide/flake-utils";

    impermanence.url = "github:nix-community/impermanence";
    sops-nix = {
      url = "github:mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
    deploy-rs = { url = "github:serokell/deploy-rs"; inputs.nixpkgs.follows = "nixpkgs"; };
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, ... }: {
    nixosModules.default = ./module.nix;
  }
  //
  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };

      dummySystem = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = { inherit inputs; };
        modules = [ self.nixosModules.default ];
      };
      optionsDoc = pkgs.nixosOptionsDoc {
        options = { inherit (dummySystem.options) homelab; };
        warningsAreErrors = false;
        # markdownByDefault = true;
      };
    in
    {
      packages.docs = pkgs.runCommandLocal "book"
        { buildInputs = [ pkgs.mdbook ]; }
        ''
          cp -R ${./.} .
          cd *source
          chmod -R 755 .
          ln -s ${optionsDoc.optionsCommonMark} "./doc/module-options.md"
          mdbook build
          mv book $out
        '';

      devShells.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          mdbook
          book-summary
          microserver
        ];
      };

      nixosConfigurations.default = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = { inherit inputs; };
        modules = [
          ./module.nix
          {
            homelab.cloud.mailer.enable = true;
            boot.isContainer = true;
          }
        ];
      };
    }
  );
}
