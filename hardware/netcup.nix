{ config, lib, ... }:

let
  name = "netcup";
  cfg = config.homelab.hardware."${name}";
  inherit (lib) mkEnableOption mkIf;
in
{

  options.homelab.hardware."${name}".enable = mkEnableOption "VPS config for ${name}";

  config = mkIf cfg.enable {
    homelab.hardware.qemu-guest.enable = true;
    boot.initrd.availableKernelModules = [ "ata_piix" "uhci_hcd" "virtio_pci" "virtio_scsi" "sd_mod" "sr_mod" "virtio_blk"];
  };
}
