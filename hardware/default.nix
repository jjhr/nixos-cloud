{
  imports = [
    ./laptop.nix
    ./hetzner.nix
    ./netcup.nix
    ./qemu-guest.nix
  ];
}
