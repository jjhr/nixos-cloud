{ config, lib, pkgs, ... }:

let
  name = "laptop";
  cfg = config.homelab.hardware."${name}";
  inherit (lib) mkEnableOption mkIf;
in
{

  options.homelab.hardware."${name}" = {
    enable = mkEnableOption "${name} related options";
    enableTouch = mkEnableOption "Touchpad & Touchscreen";
    enableBluetooth = mkEnableOption "Bluetooth";
    preserveBattery = mkEnableOption "Charge Battery only to 80%" // { default = true; };
  };

  config = mkIf cfg.enable {
    powerManagement = {
      powertop.enable = true;
      cpuFreqGovernor = "ondemand";
      powerUpCommands = ''
        ${pkgs.powertop}/bin/powertop --auto-tune
      '';
      resumeCommands = ''
        ${pkgs.powertop}/bin/powertop --auto-tune
      '';
    };

    homelab.ephemeral.persist.dirs = [ "/var/lib/NetworkManager" ];
    environment.systemPackages = with pkgs; [ powertop ];
    # networking.networkmanager.enable = true;
    # networking.wireless.enable = lib.mkForce false;
    # networking.useDHCP = lib.mkForce false;

    hardware.bluetooth.enable = cfg.enableBluetooth;
    # hardware.trackpoint = {
    #   enable = true;
    #   emulateWheel = true;
    # };

    services = {
      # touchegg.enable = true;
      libinput.enable = cfg.enableTouch;
      # xserver.libinput.touchpad.disableWhileTyping = true;
      # xserver.displayManager.sessionCommands = ''
      #   ${pkgs.xorg.xinput}/bin/xinput disable 'SynPS/2 Synaptics TouchPad'
      # '';


      tlp = {
        enable = true;
        settings = mkIf cfg.preserveBattery {
          START_CHARGE_THRESH_BAT0 = 75;
          STOP_CHARGE_THRESH_BAT0 = 80;
          # USB_BLACKLIST = [ "046d:c084" "046d:c312" ];
        };
      };

      logind = {
        lidSwitch = "lock";
        lidSwitchDocked = "ignore";
      };

    };
  };

}
