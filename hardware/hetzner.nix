{ config, lib, ... }:

let
  name = "hetzner";
  cfg = config.homelab.hardware."${name}";
  inherit (lib) mkEnableOption mkIf;
in
{

  options.homelab.hardware."${name}".enable = mkEnableOption "VPS config for ${name}";

  config = mkIf cfg.enable {
    homelab.hardware.qemu-guest.enable = true;
    boot.initrd.availableKernelModules = [ "ata_piix" "virtio_pci" "virtio_scsi" "xhci_pci" "sd_mod" "sr_mod" ];
  };
}
