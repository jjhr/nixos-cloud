{ config, lib, ... }:

let
  name = "qemu-guest";
  cfg = config.homelab.hardware."${name}";
  inherit (lib) mkEnableOption mkIf mkForce;
in
{

  options.homelab.hardware."${name}".enable = mkEnableOption "VPS config for ${name}";

  config = mkIf cfg.enable {
    boot.initrd.availableKernelModules = [ "virtio_net" "virtio_pci" "virtio_mmio" "virtio_blk" "virtio_scsi" "9p" "9pnet_virtio" ];
    boot.initrd.kernelModules = [ "virtio_balloon" "virtio_console" "virtio_rng" ];

    boot.initrd.postDeviceCommands = lib.mkIf (!config.boot.initrd.systemd.enable)
      ''
        # Set the system time from the hardware clock to work around a
        # bug in qemu-kvm > 1.5.2 (where the VM clock is initialised
        # to the *boot time* of the host).
        hwclock -s
      '';

    i18n.supportedLocales = [ (config.i18n.defaultLocale + "/UTF-8") ];
    documentation.enable = mkForce false;
    documentation.nixos.enable = mkForce false;
    programs.command-not-found.enable = mkForce false;

    hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  };
}
