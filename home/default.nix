{ inputs, ... }:
# let
  # inherit (lib) mkOption types mkIf
  #   mapAttrs flip attrValues any
    # ;

  # inherit (config.homelab) users;
  # hmEnabled = flip any
  #   (attrValues users)
  #   ({ home-manager, ... }: home-manager != null);
# in
{

  imports = [ inputs.home-manager.nixosModule ];

  # options.homelab.users = mkOption {
  #   type = types.attrsOf (types.submodule ({ name, ... }: {
  #     options = {
  #       user = mkOption {
  #         type = types.attrs;
  #         description = ''
  #           Shorthand for users.users.${name}.
  #         '';
  #         default = { };
  #       };
  #       home-manager = mkOption {
  #         type = types.nullOr types.raw;
  #         default = null;
  #       };
  #     };
  #   }));
  #   default = { };
  # };

  config = {
    # users.users = flip mapAttrs users (_: { user, ... }: user);

    home-manager = {
      useGlobalPkgs = true;
      useUserPackages = true;
      extraSpecialArgs = { inherit inputs; };
      # users = mkIf hmEnabled (flip mapAttrs users (_: { home-manager, ... }: home-manager));
    };
  };

}
