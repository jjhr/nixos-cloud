{ config, options, lib, ... }:

let
  cfg = config.homelab.ephemeral.automagic;
  inherit (lib) mkEnableOption mkIf optional;
in
{

  options.homelab.ephemeral.automagic.enable = mkEnableOption "auto persist / backup";

  config = mkIf cfg.enable {
    homelab.ephemeral.backup.dirs =
      optional config.services.fail2ban.enable "/var/lib/fail2ban"
    ;
  };

}
