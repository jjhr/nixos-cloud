{ config, lib, inputs, ... }:

let
  cfg = config.homelab.ephemeral;
  inherit (lib) mkEnableOption mkOption mkIf mkForce mkAfter types mkMerge
    mapAttrs flip;

  filesetOpts = {
    dirs = mkOption {
      type = types.listOf (types.either types.nonEmptyStr types.attrs);
      default = [ ];
      description = ''
        Paths like "/var/lib/syncthing" or attrsets like 
        { 
          directory = "/var/lib/syncthing"; 
          user = "syncthing"; 
          group = "syncthing"; 
          mode = "0700"; 
        }
      '';
    };
    files = mkOption {
      type = types.listOf types.nonEmptyStr;
      default = [ ];
    };
  };
in
{
  imports = [
    inputs.impermanence.nixosModule

    ./automagic.nix
  ];

  options.homelab = {
    ephemeral = {
      enable = mkEnableOption "ephemeral ZFS";

      mode = mkOption {
        type = types.enum [ "tmpfs" "rollback" ];
        description = ''
          Where the ephemeral data is stored and how it is reset.
            tmpfs -> data in RAM, wiped when power off.
            rollback -> data on ZFS dataset, rollback on each boot
        '';
      };
      tmpfsSize = mkOption { type = types.nonEmptyStr; default = "3G"; };

      persist = filesetOpts; # TODO // { home = mkFilesetOpts; };
      backup = filesetOpts; # TODO // { home = mkFilesetOpts; };
    };

    users = mkOption {
      type = types.attrsOf (types.submodule {
        options = {
          ephemeral = {
            persist = filesetOpts;
            backup = filesetOpts;
          };
        };
      });
    };
  };

  config = mkMerge [
    {
      homelab.ephemeral.backup.dirs = [
        "/var/lib/nixos"
      ];
      homelab.ephemeral.persist.dirs = [
        "/var/log"
        "/var/lib/systemd/coredump"
      ];

      home-manager.sharedModules = [{
        options = {
          homelab.ephemeral = {
            persist = filesetOpts;
            backup = filesetOpts;
          };
        };
      }];
    }

    (mkIf cfg.enable {
      fileSystems = {
        "/" =
          if cfg.mode == "tmpfs"
          then
            {
              device = "none";
              fsType = "tmpfs";
              options = [ "defaults" "size=${cfg.tmpfsSize}" "mode=755" ];
            }
          else
            {
              device = "nixpool/ephemeral";
              fsType = "zfs";
            };

        "/nix" = { device = "nixpool/nix"; fsType = "zfs"; };

        "/persist" = {
          device = "nixpool/persist";
          fsType = "zfs";
          neededForBoot = true; # Needed for impermanence bind mounts
        };
        "/backup" = {
          device = "nixpool/backup";
          fsType = "zfs";
          neededForBoot = true; # Needed for impermanence bind mounts
        };
      };

      boot = {
        supportedFilesystems = [ "zfs" ];
        zfs.requestEncryptionCredentials = true;

        # Hibernation when ZFS on root can cause data corruption (https://github.com/openzfs/zfs/issues/260)
        # TODO can be enabled when swap is not on ZFS
        kernelParams = [ "nohibernate" ];

        initrd.postDeviceCommands = mkIf (cfg.mode == "rollback") (mkAfter ''
          zfs rollback -r nixpool/ephemeral@blank
        '');
      };

      services = {
        zfs = {
          autoScrub.enable = true;
          autoSnapshot.enable = true;
        };

        openssh.hostKeys = mkForce [
          { type = "ed25519"; path = "/backup/etc/ssh/ssh_host_ed25519_key"; }
        ];
      };

      environment.persistence =
        let
          mkUsers = name: flip mapAttrs config.home-manager.users (_: uCfg: {
            directories = uCfg.homelab.ephemeral.${name}.dirs;
            inherit (uCfg.homelab.ephemeral.${name}) files;
          });
        in
        {
          "/persist" = {
            hideMounts = true;
            directories = cfg.persist.dirs;
            inherit (cfg.persist) files;
            users = mkUsers "persist";
          };
          "/backup" = {
            hideMounts = true;
            directories = cfg.backup.dirs;
            inherit (cfg.backup) files;
            users = mkUsers "backup";
          };
        };

    })
  ];

}
