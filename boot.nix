{ config, lib, ... }:
let
  cfg = config.homelab.boot;
  inherit (lib) mkEnableOption mkOption mkMerge mkIf types;
in
{
  options.homelab.boot = {
    firmware = mkOption { type = types.enum [ "uefi" "bios" ]; };
    grubDisk = mkOption { type = types.str; };
    bootDevice = mkOption { type = types.nonEmptyStr; default = "/dev/disk/by-label/BOOT"; };
    skipGrub = mkEnableOption "shorter grub screen. Hold shift to disable";
  };

  config = {
    fileSystems."/boot" = { device = cfg.bootDevice; fsType = "vfat"; };

    boot.loader = mkMerge [
      (mkIf (cfg.firmware == "uefi")
        {
          efi.canTouchEfiVariables = true;
          grub = {
            efiSupport = true;
            devices = [ "nodev" ];
            useOSProber = true;
          };
        })

      (mkIf (cfg.firmware == "bios")
        { grub.devices = [ cfg.grubDisk ]; }
      )

      (mkIf cfg.skipGrub {
        timeout = 0;
        grub.extraConfig = ''
          if keystatus --shift ; then
            set timeout=-1
          else
            set timeout=0
          fi
        '';
      })

    ];
  };
}
