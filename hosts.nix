{ lib, options, ... }:
let
  inherit (lib) mkOption types
  # hasAttr
  ;
  # cfg = config.homelab.hosts;
in
{

  options.homelab.hosts = mkOption {
    type = types.attrsOf (types.submodule {
      options = {
        inherit (options.homelab) tailnet;
      };
    });
    description = ''
      Internal network configuration
    '';
    default = { };
  };

  # config.homelab.tailnet =
  #   if hasAttr config.homelab.hostName cfg
  #   then cfg.${config.homelab.hostName}.tailnet
  #   else { };

}
