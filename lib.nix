{ lib, config, ... }:
let
  inherit (lib) mkOption types
    hasAttr head tail pipe filter foldl' recursiveUpdate attrNames findFirst hasSuffix
    zipAttrsWith last unique all isList concatLists isAttrs
    sort stringLength;

in
{
  options.homelab.lib = mkOption {
    type = types.attrsOf types.anything;
    description = "homelab library";
    readOnly = true;
  };

  config.homelab.lib = rec {

    attrPath = path: attr:
      if path == [ ] then attr
      else if hasAttr (head path) attr then attrPath (tail path) attr."${head path}"
      else null;

    mergeAttrList = foldl' recursiveUpdate { };
    mergeAttrListConcatLists = attrList:
      let
        f = attrPath:
          zipAttrsWith (n: values:
            if tail values == [ ]
            then head values
            else if all isList values
            then unique (concatLists values)
            else if all isAttrs values
            then f (attrPath ++ [ n ]) values
            else last values
          );
      in
      f [ ] attrList;

    filterTryEval = xs: pipe xs [
      (map (x: builtins.tryEval (builtins.deepSeq x x)))
      (filter ({ success, ... }: success))
      (map ({ value, ... }: value))
    ];

    findWildcardDomain = domain:
      pipe config.homelab.cloud.nginx.wildcardDomains [
        attrNames
        (filter (parent: hasSuffix parent domain))
        (sort (x: y: stringLength x > stringLength y))
        (findFirst (_: true) null)
      ];

    acmeForDomain = domain:
      let parentDomain = findWildcardDomain domain;
      in if parentDomain == null
      then { enableACME = true; }
      else { useACMEHost = parentDomain; };

  };

}
