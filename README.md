# Homelab.nixos

An opinionated "NixOS Distro" with modules for Servers and Desktops[^1].
Automates homelab-internal configuration like SSL certs, DNS setup and more via
a shared module with `homelab.hosts`.

### [🔗 Module options](https://jjhr.gitlab.io/homelab.nixos)

### Features

- Flakes first
- ZFS
    - ephemeral storage with either snapshots or tmpfs
    - remote unlock ZFS full disk encryption via SSH[^1]
- Pre-defined `hardware-configuration.nix` for common VPSs / Hardware (`homelab.hardware`)
- Deployments / Updates
    - Remote via deploy-rs / SSH
    - Pull-deployment[^1] via SSH / nixos-rebuild cron job
- sops for secrets
- intranet (`homelab.cloud`)
    - via headscale / tailscale (auto login coming soon)
    - internal DNS
    - internal only SSL certs via Lets Encrypt DNS challenge
    - shared module between hosts `homelab.hosts`
        - automagic DNS setup
        - automagic SSL certs
        - automagic dashboard with all available services
        - automagic metric collection
        - automagic HTTP(S) tunneling via tailscale
    - SSO (via lldap)
    - Automatic setup for notification mails for services
    - Services:
        - Headscale control panel
        - vaultwarden
        - Gitlab (+Runners)
        - Gitea
        - Nextcloud
        - Monitoring / alerting with prometheus & grafana[^1]
    - Backups[^1]
        - Push / Pull via sanoid / syncoid
        - Push via Borg
- Desktops / Graphical [^1]
    - Minimal TWM / "power user"
        - XMonad, nvim, lf, neomutt, newsboat, neovim, qutebrowser, firefox (arkenfox, plugins, theme)
    - Simple Desktop (XFCE / LXQt / KDE)
        - nixos-software-center, nixos-conf-editor
        - Office Suite
        - Wine for gaming
- Pre configured programs / utility modules[^1]
    - inspired by voidrice:
        - lfimg
        - neomutt module
    - firejail wrapper for browsers, etc
    - isolated ungoogled-chromium for webapps

### Uses

- deploy-rs
- sops-nix
- home-manager (for desktop only)

[^1]: coming soon

