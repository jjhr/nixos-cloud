{ config, lib, pkgs, ... }:

let
  cfg = config.services.xlayoutdisplay;
  inherit (lib) mkEnableOption mkIf mkOption types;
in
{

  options.services.xlayoutdisplay = {
    enable = mkEnableOption "options for desktop usage";
    dpi = mkOption {
      type = types.nullOr types.ints.u16;
      default = null;
    };
    postHook = mkOption {
      type = types.lines;
      description = "Shell script to run after displays are layed out.";
      default = "";
    };
  };

  config = mkIf cfg.enable {

    services.udev.extraRules = ''
      ACTION=="change", SUBSYSTEM=="drm", ENV{DISPLAY}=":0", TAG+="systemd", ENV{SYSTEMD_USER_WANTS}="xlayoutdisplay.service"
    '';

    services.acpid = {
      inherit (config.homelab.hardware.laptop) enable;
      lidEventCommands = ''
        DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus" ${pkgs.doas}/bin/doas -u ${config.homelab.desktop.defaultUser} ${pkgs.systemd}/bin/systemctl --user start xlayoutdisplay.service
      '';
    };

    home-manager.sharedModules = [{
      xsession.profileExtra = ''
        systemctl --user start xlayoutdisplay.service
      '';
    }];

    environment.systemPackages = [ pkgs.xlayoutdisplay ];

    systemd.user.services.xlayoutdisplay = {
      path = with pkgs; [ coreutils xlayoutdisplay xorg.xrdb xorg.xrandr bash ];
      script = ''
        sleep .5
        xlayoutdisplay ${lib.optionalString (cfg.dpi != null) "--dpi ${toString cfg.dpi}"}
        bash -l ${pkgs.writeScript "xlayoutdisplay-post-hook" cfg.postHook}
      '';
      serviceConfig.Type = "oneshot";
    };
  };
}
