{ lib, config, ... }:
let
  cfg = config.homelab.desktop.samba;
  inherit (lib) mkEnableOption mkOption mkIf types
    mapAttrs;
in
{
  options.homelab.desktop.samba = {
    enable = mkEnableOption "samba exposed to local network";
    enableWindowsDiscovery = mkEnableOption "samba to be discovered by windows pcs";
    shares = mkOption {
      type = types.attrsOf types.str;
      default = { };
    };
  };

  config = mkIf cfg.enable {

    homelab.ephemeral.persist.dirs = [ "/var/lib/samba" ];

    # https://opensource.com/article/21/4/share-files-linux-windows
    services.samba = {
      enable = true;
      openFirewall = true;
      securityType = "user";
      shares = mapAttrs
        (_: path: {
          inherit path;
          browseable = "yes";
          "read only" = "no";
          "writable" = "yes";
        })
        cfg.shares;
      settings.global = {
        "workgroup" = "SAMBA";
        "security" = "user";
        "log file" = "/var/log/samba/smbd.%m";
        "guest account" = "nobody";
        "map to guest" = "Bad User";
      };
    };
    services.samba-wsdd = lib.mkIf cfg.enableWindowsDiscovery {
      enable = true;
      openFirewall = true;
      workgroup = "SAMBA";
      listen = "0.0.0.0";
      discovery = true;
    };
  };
}
