{ pkgs, lib, config, ... }:
let
  cfg = config.homelab.desktop.vms;
  inherit (lib) mkEnableOption mkIf;
in
{
  options.homelab.desktop.vms.enable = mkEnableOption "virtual machines via libvirt";

  config = mkIf cfg.enable {

    homelab.desktop.groups = [ "libvirtd" "qemu-libvirtd" ];

    environment.systemPackages = with pkgs; [ virt-manager cdrtools ];

    virtualisation.libvirtd = {
      enable = true;
      qemu.ovmf.enable = true;
      qemu.runAsRoot = false;
      onBoot = "ignore";
      onShutdown = "shutdown";
    };

    homelab.ephemeral.persist.dirs = [ "/var/lib/libvirt" ];
  };
}
