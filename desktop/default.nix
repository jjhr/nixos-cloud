{ config, lib, pkgs, ... }:

let
  cfg = config.homelab.desktop;
  inherit (lib) mkMerge mkDefault mkEnableOption mkIf mkOption types
    listToAttrs flip nameValuePair;
in
{
  imports = [
    ./audio.nix
    ./samba.nix
    ./vms.nix
    ./xlayoutdisplay.nix
  ];

  options.homelab.desktop = {
    enable = mkEnableOption "options for desktop usage";
    defaultUser = mkOption {
      type = types.str;
      description = ''
        Default user for autologin etc.
      '';
    };
    users = mkOption {
      type = types.listOf types.str;
      description = ''
        Desktop users which will be added to homelab.desktop.groups.
      '';
      default = [ ];
    };
    groups = mkOption {
      type = types.listOf types.str;
      description = ''
        Default groups for desktop users
      '';
      default = [ ];
    };
  };

  config = mkMerge [
    {
      homelab.desktop = {
        groups = [ "wheel" ];
        users = [ cfg.defaultUser ];
      };
    }
    (mkIf cfg.enable {

      homelab.desktop.audio.enable = true;

      security.polkit.enable = true;

      users.users = listToAttrs (flip map cfg.users
        (user: nameValuePair user {
          isNormalUser = true;
          extraGroups = cfg.groups;
        }));

      hardware.graphics = mkDefault {
        enable = true;
        enable32Bit = true;
        # driSupport = true;
        # driSupport32Bit = true;
        # TODO check
        extraPackages = with pkgs; [
          intel-media-driver # LIBVA_DRIVER_NAME=iHD
          vaapiIntel # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
          vaapiVdpau
          libvdpau-va-gl
        ];
      };

      # TODO modularize
      # services.opensnitch.enable = true;
      # pkgs.opensnitch-ui

      environment.systemPackages = [
        pkgs.ddcutil
      ];

      services = {
        printing.enable = true;
        avahi = {
          enable = true;
          nssmdns4 = true;
        };

        xserver = {
          enable = true;
          # upscaleDefaultCursor = true;
        };

        displayManager.autoLogin = {
          enable = true;
          user = cfg.defaultUser;
        };
        xlayoutdisplay.enable = true;

        # TODO
        smartd = {
          enable = true;
          notifications = {
            test = true;
            mail.enable = true;
            mail.recipient = "mail@CHANGEME.de";
            x11.enable = true;
          };
        };

        udisks2.enable = true;
        upower.enable = true;
      };
    })
  ];
}
