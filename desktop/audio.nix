{ config, lib, pkgs, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.homelab.desktop.audio;
in
{

  options.homelab.desktop.audio = {
    enable = mkEnableOption "audio with pipewire and wireplumber. Autosuspend is disabled for wireplumber";
  };

  config = mkIf cfg.enable {
    homelab.desktop.groups = [ "audio" ];

    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;

      wireplumber.enable = true;
      wireplumber.configPackages = [
        (pkgs.writeTextDir "share/wireplumber/wireplumber.conf.d/alsa.conf"  ''
          monitor.alsa.rules = [
            {
              matches = [
                { device.name = "~alsa_card.*" }
              ]
              actions = {
                update-props = {
                  # Device settings
                  api.alsa.use-acp = true
                }
              }
            }
            {
              matches = [
                { node.name = "~alsa_input.pci.*" }
                { node.name = "~alsa_output.pci.*" }
              ]
              actions = {
                # Node settings
                update-props = {
                  session.suspend-timeout-seconds = 0
                }
              }
            }
          ]
        '')
      ];
    };

    environment.systemPackages = with pkgs; [
      # pulsemixer
      pavucontrol
      qjackctl
      # easyeffects
    ];



  };
}
