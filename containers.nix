{ config, lib, pkgs, ... }:
let
  cfg = config.homelab.containers;
  inherit (lib) mkOption mkMerge mkIf mkEnableOption types;
in
{
  options.homelab.containers = {
    enable = mkOption {
      type = types.enum [ false "podman" "docker" ];
      default = false;
      description = ''
        Whether to enable an oci-runtime on ZFS.
      '';
    };
    enableNvidia = mkEnableOption "nvidia support";
  };

  config = mkMerge [

    (mkIf (cfg.enable == "podman") {
      homelab.desktop.groups = [ "podman" ];
      virtualisation.oci-containers.backend = "podman";
      virtualisation.podman = {
        enable = true;
        inherit (cfg) enableNvidia;

        # extraPackages = [ pkgs.zfs ];
        dockerSocket.enable = true;

        # Required for containers under podman-compose to be able to talk to each other.
        defaultNetwork.settings.dns_enabled = true;
      };

      virtualisation.containers.storage.settings.storage = {
        driver = "zfs";
        graphroot = "/persist/var/lib/containers/storage";
        runroot = "/run/containers/storage";
      };

      environment.systemPackages = [ pkgs.docker-client ];

    })

    (mkIf (cfg.enable == "docker") {
      homelab.desktop.groups = [ "docker" ];
      virtualisation.oci-containers.backend = "docker";
      virtualisation.docker = {
        enable = true;
        inherit (cfg) enableNvidia;
        storageDriver = "zfs";
        daemon.settings.data-root = "/persist/var/lib/docker";
      };
    })

  ];
}
