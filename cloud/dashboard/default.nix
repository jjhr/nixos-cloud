{ config, lib, pkgs, ... }:

let
  service = "dashboard";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkOption mkEnableOption types mdDoc
    recursiveUpdate mapAttrsToList pipe;

  inherit (config.homelab.lib) mergeAttrListConcatLists;

  settingsFormat = pkgs.formats.yaml { };
in
{

  imports = [ ./catalog.nix ];

  options.homelab.cloud.${service} = {

    enable = mkEnableOption "homer, the static HOMe servER dashboard";
    domain = mkOption { type = types.str; example = "home.test.com"; };
    settings = mkOption {
      inherit (settingsFormat) type;
      description = mdDoc ''
        Homer settings. 
        See [homer example config](https://github.com/bastienwirtz/homer/blob/main/public/assets/config-demo.yml.dist).
      '';
      default = { };
    };
    entries = mkOption {
      type = types.attrsOf types.str;
      description = ''
        Dashboard entries to show. Attr name is the service for which the entry
        is shown and attr value is the domain to refer to.
      '';
      example = {
        "vpn.server" = "hs.test.de";
        "vaultwarden" = "vault.internal.test.de";
      };
      default = { };
    };

  };

  config = lib.mkIf cfg.enable {

    services.nginx.virtualHosts."${cfg.domain}" =
      config.homelab.lib.acmeForDomain cfg.domain
      // {
        addSSL = true;

        locations."=/assets/config.yml".alias =
          let
            allHostsServices =
              pipe cfg.entries [
                (mapAttrsToList (name: domain:
                  if
                    !(builtins.hasAttr name cfg.catalog)
                  then
                    builtins.throw ''No homelab.cloud.dashboard.catalog entry exists with name ${name}. 
                    Available names: ${toString (builtins.attrNames cfg.catalog)}''
                    else cfg.catalog."${name}" domain
                    ))
                mergeAttrListConcatLists
                (mapAttrsToList (name: service: service // { inherit name; }))
              ];

            settings = recursiveUpdate cfg.settings { services = allHostsServices; };
          in
          settingsFormat.generate "config.yml" settings;

        locations."/".root = pkgs.fetchzip {
          url = "https://github.com/bastienwirtz/homer/releases/download/v23.02.2/homer.zip";
          sha256 = "sha256-/YUuv5kctjVbtoo0bhSwTKc5NFpkA7mwCllwESl/bVI=";
          stripRoot = false;
        };
      };
  };
}
