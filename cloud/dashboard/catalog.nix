{ lib, pkgs, ... }:
let 
  inherit (lib) mkOption types mdDoc;
  settingsFormat = pkgs.formats.yaml { };
  servicesOpt = mkOption {
    type = types.attrsOf (types.submodule {
      freeformType = settingsFormat.type;

      options.items = mkOption {
        type = types.listOf settingsFormat.type;
      };
    });
    description = mdDoc ''
      Homer services settings where attrname becomes service.name.
      See [homer example config](https://github.com/bastienwirtz/homer/blob/main/public/assets/config-demo.yml.dist).
    '';
    default = { };
  };

in
{

  options.homelab.cloud.dashboard.catalog = mkOption {
    type = types.attrsOf (types.functionTo servicesOpt.type);
    description = mdDoc ''
      Catalog of Homer service generators, associated with a homelab
      service. Each generator is called with its services config, if a given
      service is enabled on a host. Config inside a generator works as in
      `config.homelab.cloud.dashboard.services`.
    '';
    default = { };
  };


  config.homelab.cloud.dashboard.catalog = {
    vaultwarden = domain: {
      Administration.items = [{
        name = "Password Manager (Vaultwarden)";
        url = "https://${domain}";
        logo = "https://raw.githubusercontent.com/bitwarden/brand/master/icons/128x128.png";
      }];
    };

    "vpn.server" = domain: {
      Administration.items = [{
        name = "VPN Control Panel (Headscale)";
        url = "https://${domain}/web";
        logo = "https://asset.brandfetch.io/id7QyaLp8E/idYJnR6YE1.jpeg";
      }];
    };

    ldap = domain: {
      Administration.items = [{
        name = "Users and Groups (LLDAP)";
        url = "https://${domain}";
        icon = "fa fa-users";
      }];
    };

    gitea = domain: {
      Code.items = [{
        name = "Gitea";
        url = "https://${domain}";
        logo = "https://docs.gitea.io/images/gitea.png";
      }];
    };

    gitlab = domain: {
      Code.items = [{
        name = "Gitlab";
        url = "https://${domain}";
        logo = "https://about.gitlab.com/images/press/press-kit-icon.svg";
      }];
    };

    forgejo = domain: {
      Code.items = [{
        name = "Forgejo (Git)";
        url = "https://${domain}";
        logo = "https://forgejo.org/images/forgejo-wordmark.svg";
      }];
    };

    nextcloud = domain: {
      Services.items = [{
        name = "Nextcloud";
        url = "https://${domain}";
        logo = "https://nextcloud.com/wp-content/uploads/2022/10/nextcloud-logo-blue-transparent.svg";
      }];
    };

  };
}
