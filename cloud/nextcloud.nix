{ config, lib, pkgs, options, ... }:

let
  service = "nextcloud";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkIf mkMerge mkOption mkForce mkEnableOption types
    optional splitString elemAt;

  inherit (config.homelab.cloud) mailer;
in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service // {
      description = ''
        Whether to enable ${service}.

        Required secrets:
        - `cloud/nextcloud/root`: nextcloud root user password

        Manual setup (ldap):
        See <https://github.com/nitnelave/lldap/blob/main/example_configs/nextcloud.md>
      '';
    };
    domain = mkOption { type = types.str; example = "cloud.test.com"; };
    tunnel = mkOption {
      type = types.nullOr (types.submodule {
        options = {
          host = mkOption {
            type = types.str;
            description = ''
              Tailnet ip of host in homelab, which should tunnel https to ${service}.
            '';
          };
          domain = mkOption { type = types.str; example = "cloud.test.net"; };
        };
      });
      default = null;
    };
    inherit (options.homelab.cloud) ldap;
  };

  config = mkIf cfg.enable (mkMerge [

    {

      homelab.ephemeral.backup.dirs = [{
        directory = "/var/lib/nextcloud";
        user = config.users.users.nextcloud.name;
        group = config.users.users.nextcloud.name;
      }];

      sops.secrets = {
        # "cloud/nextcloud/db".owner = config.users.users.nextcloud.name;
        "cloud/nextcloud/root".owner = config.users.users.nextcloud.name;
      };

      services.nginx = {
        appendHttpConfig = "server_names_hash_bucket_size 64;";

        virtualHosts."${cfg.domain}" =
          config.homelab.lib.acmeForDomain cfg.domain
          // { forceSSL = true; };
      };


      users.users.nextcloud.extraGroups = [ config.users.groups.keys.name ];

      services.nextcloud = {
        enable = true;
        # package = pkgs.nextcloud28;
        hostName = cfg.domain;
        https = true;

        autoUpdateApps.enable = true;

        phpExtraExtensions = all:
          optional (cfg.ldap != null) all.ldap;


        # Enable caching using redis https://nixos.wiki/wiki/Nextcloud#Caching.
        configureRedis = true;
        # https://docs.nextcloud.com/server/26/admin_manual/configuration_server/caching_configuration.html
        caching.redis = true;

        # Adds appropriate nginx rewrite rules.
        webfinger = true;

        config = {
          dbtype = "pgsql";
          dbuser = "nextcloud";
          dbhost = "/run/postgresql";
          dbname = "nextcloud";
          # dbpassFile = config.sops.secrets."cloud/nextcloud/db".path;
          adminpassFile = config.sops.secrets."cloud/nextcloud/root".path;
          adminuser = "root";

          # Further forces Nextcloud to use HTTPS
          overwriteProtocol = "https";
          defaultPhoneRegion = "DE";
        };

        database.createLocally = true;

        extraOptions = {
          "overwrite.cli.url" = "https://${cfg.domain}";
          overwritehost = cfg.domain;
        };

        nginx.hstsMaxAge = 31536000; # Needs > 1 year for https://hstspreload.org to be happy

        phpOptions = {
          # The OPcache interned strings buffer is nearly full with 8, bump to 16.
          catch_workers_output = "yes";
          display_errors = "stderr";
          error_reporting = "E_ALL & ~E_DEPRECATED & ~E_STRICT";
          expose_php = "Off";
          "opcache.enable_cli" = "1";
          "opcache.fast_shutdown" = "1";
          "opcache.interned_strings_buffer" = "16";
          "opcache.max_accelerated_files" = "10000";
          "opcache.memory_consumption" = "128";
          "opcache.revalidate_freq" = "1";
          "openssl.cafile" = "/etc/ssl/certs/ca-certificates.crt";
          short_open_tag = "Off";

          # Needed to avoid corruption per https://docs.nextcloud.com/server/21/admin_manual/configuration_server/caching_configuration.html#id2
          "redis.session.locking_enabled" = "1";
          "redis.session.lock_retries" = "-1";
          "redis.session.lock_wait_time" = "10000";
        };
      };
    }

    (mkIf mailer.enable (
      let
        secretFile = "/var/lib/nextcloud/config/secrets.json";
        mailSplit = splitString "@" mailer.user;
      in
      {
        services.nextcloud = {
          inherit secretFile;

          extraOptions = {
            mail_smtpmode = "smtp";
            mail_smtpsecure = "ssl";
            mail_smtpauthtype = "LOGIN";
            mail_smtpauth = true;
            mail_smtphost = mailer.host;
            mail_smtpport = mailer.port;
            mail_smtpname = mailer.user;
            mail_from_address = elemAt mailSplit 0;
            mail_domain = elemAt mailSplit 1;
          };
        };

        systemd.services."nextcloud-setup".preStart = ''
          cat << EOF > ${secretFile}
            {
              "mail_smtppassword": "$(cat ${mailer.passFile})"
            }
          EOF
        '';
      }
    ))

    (mkIf (cfg.tunnel != null) {
      services.nextcloud = {
        config = {
          extraTrustedDomains = [ cfg.tunnel.domain ];
          trustedProxies = [ "127.0.0.1" cfg.tunnel.host ];
        };
        extraOptions = {
          overwritehost = mkForce cfg.tunnel.domain;
          "overwrite.cli.url" = mkForce "https://${cfg.tunnel.domain}";
        };
      };
    })
  ]);
}
