{ pkgs, lib, config, ... }:
{
  services.postgresql.package = pkgs.postgresql_14;
  homelab.ephemeral.backup.dirs = lib.mkIf config.services.postgresql.enable
    [ "/var/lib/postgresql" ];
}
