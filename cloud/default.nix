{
  imports = [

    ./backup.nix
    ./dashboard
    ./forgejo.nix
    ./forgejo-actions-runner.nix
    ./forgejo-pages.nix
    ./gitlab.nix
    ./gitlab-runner.nix
    ./ldap.nix
    ./metrics.nix
    ./nextcloud.nix
    ./nginx.nix
    ./mailer.nix
    ./postgres.nix
    ./vaultwarden.nix

    ./vpn/client.nix
    ./vpn/server.nix
  ];
}
