{ config, lib, pkgs, ... }:

let
  service = "ldap";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkEnableOption mkOption mdDoc mkIf mkMerge types
    pipe concatStringsSep splitString hasPrefix;

  lldapConfigFormat = pkgs.formats.toml { };
  superDomain = config.homelab.lib.findWildcardDomain cfg.domain;
  certsDir = config.security.acme.certs."${superDomain}".directory;

in
{

  options.homelab.cloud.${service} = {

    enable = mkEnableOption "SSO with lldap";

    domain = mkOption {
      type = types.str;
      description = "HTTP endpoint";
      example = "ldap.test.com";
    };

    baseDn = mkOption {
      type = types.str;
      description = ''
        LDAP base dn, formatted like a domain name. Will be split by '.'.
        This domain should be the same or a superdomain of
        `homelab.cloud.ldap.domain` in order to get ldaps working.
      '';
      example = "test.com";
      apply = domain:
        if hasPrefix "dc" domain
        then domain
        else
          pipe domain [
            (splitString ".")
            (map (dc: "dc=${dc}"))
            (concatStringsSep ",")
          ];
    };

    settings = mkOption {
      inherit (lldapConfigFormat) type;
      description = mdDoc ''
        See [default lldap config](https://github.com/nitnelave/lldap/blob/main/lldap_config.docker_template.toml).
      '';
      default = { };
    };

  };

  config = mkMerge [

    {
      homelab.cloud.ldap.settings = {
        # only ldaps will be allowed through firewall
        ldap_host = config.homelab.tailnet.ip;
        ldap_port = 3890;
        http_host = "127.0.0.1";
        http_port = 17170;

        http_url = "https://${cfg.domain}";
        ldap_base_dn = cfg.baseDn;
        ldap_user_dn = "root";
        ldap_user_email = "root@${cfg.baseDn}";
        database_url = "sqlite:///var/lib/lldap/users.db?mode=rwc";
        key_file = "/var/lib/lldap/private-key";

        ldaps_options = {
          enabled = true;
          port = 6360;
          cert_file = "/var/lib/lldap/cert.pem";
          key_file = "/var/lib/lldap/key.pem";
        };
      };
    }

    (mkIf cfg.enable {

      homelab.ephemeral.backup.dirs = [{ directory = "/var/lib/lldap"; user = "lldap"; group = "lldap"; }];

      # openssl rand -base64 64 / pass generate
      sops.secrets."cloud/ldap/jwt".owner = "lldap";
      sops.secrets."cloud/ldap/root_pass".owner = "lldap";

      networking.firewall.interfaces."${config.services.tailscale.interfaceName}".allowedTCPPorts =
        [ cfg.settings.ldaps_options.port ];

      services.nginx.virtualHosts."${cfg.domain}" =
        config.homelab.lib.acmeForDomain cfg.domain
        // {
          forceSSL = true;
          locations."/".proxyPass = "http://localhost:${toString cfg.settings.http_port}";
        };


      users.users.lldap = {
        group = "lldap";
        home = "/var/lib/lldap";
        createHome = true;
        isSystemUser = true;
        extraGroups = [ "acme" ];
      };
      users.groups.lldap = { };

      environment.systemPackages = [ pkgs.lldap ];


      systemd.services.lldap = {
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        preStart = ''
          ln -sf ${pkgs.lldap}/app /var/lib/lldap
          ln -sf ${lldapConfigFormat.generate "lldap_config.toml" cfg.settings} /var/lib/lldap/lldap_config.toml

          # Convert / copy keys
          ${pkgs.openssl}/bin/openssl pkcs8 -topk8 -nocrypt -in ${certsDir}/key.pem -out /var/lib/lldap/key.pem
          cp ${certsDir}/cert.pem /var/lib/lldap/cert.pem
          chown lldap:lldap /var/lib/lldap/{key,cert}.pem
        '';
        serviceConfig = {
          ExecStart = "${pkgs.lldap}/bin/lldap run";
          LimitNOFILE = 1048576;
          LimitNPROC = 64;
          NoNewPrivileges = true;
          PrivateDevices = true;
          PrivateTmp = true;
          ProtectHome = true;
          ProtectSystem = "full";
          WorkingDirectory = "/var/lib/lldap";
          ReadWriteDirectories = "/var/lib/lldap";
          Restart = "on-failure";
          Type = "simple";
          User = "lldap";
          Group = "lldap";
        };
        environment = {
          LLDAP_JWT_SECRET_FILE = config.sops.secrets."cloud/ldap/jwt".path;
          LLDAP_LDAP_USER_PASS_FILE = config.sops.secrets."cloud/ldap/root_pass".path;
        };
        restartTriggers = [ "${certsDir}/key.pem" "${certsDir}/cert.pem" ];
      };

    })
  ];
}
