{ config, lib, pkgs, ... }:

let
  cfg = config.homelab.cloud.vpn.server;
  inherit (lib) mkEnableOption mkOption mkIf types;
in
{

  options.homelab.cloud.vpn.server = {
    enable = mkEnableOption "Headscale control server with Unbound for internal DNS";

    domain = mkOption {
      type = types.str;
      description = ''
        Domain on which headscale should be served. Must be
        reachable from the internet!
      '';
      example = "hs.test.com";
    };

    extraARecords = mkOption {
      type = types.attrsOf types.str;
      description = ''
        Domains to redirect to a given host ip via A records. Important: All
        subdomains are redirected to given IP.
      '';
      example = ''{ "lab.local" = "123.123.123.123"; }'';
      default = { };
    };

    extraZones = mkOption {
      type = types.listOf types.str;
      description = "Extra local-zones (unbound)";
      example = [ ''"_acme-challenge.test.com" transparent'' ];
      default = [ ];
    };
    extraData = mkOption {
      type = types.listOf types.str;
      description = "Extra local-data (unbound)";
      example = [ "_acme-challenge.test.com IN NS ns1.test.com." ];
      default = [ ];
    };
  };

  config = mkIf cfg.enable {

    homelab.ephemeral = {
      backup.dirs = [ "/var/lib/headscale" ];
      persist.dirs = [ "/var/lib/unbound" ];
    };

    environment.systemPackages = [ pkgs.headscale ];

    services = {
      headscale = {
        enable = true;
        address = "0.0.0.0";
        port = 23578;

        settings = {
          server_url = "https://${cfg.domain}";
          ip_prefixes = [ "100.64.0.0/10" ];

          dns = {
            magic_dns = true;
            base_domain = "tailnet";
            override_local_dns = true;
            nameservers.global = [
              (if config.homelab.cloud.vpn.client.enable
              then config.homelab.tailnet.ip
              else "176.9.93.198")
            ];
          };
          log.level = "info";
          logtail.enabled = false;
        };

        # aclPolicyFile = pkgs.writeText "acls.yaml" (builtins.toJSON {
        #   acls = [
        #     { action = "accept"; src = [ "*" ]; dst = [ "*:*" ]; }
        #   ];
        # });
      };

      nginx.virtualHosts."${cfg.domain}" = {
        forceSSL = true;
        # Must be accessible from internet => always use HTTP challenge
        enableACME = true;

        locations."/web".root = pkgs.fetchzip {
          url = "https://github.com/gurucomputing/headscale-ui/releases/download/2023.01.30-beta-1/headscale-ui.zip";
          sha256 = "sha256-yeexxvwN2RksAyz0pm1viJkCyFwb9gQTzLDJwS9WNiE=";
          stripRoot = false;
        };

        locations."/" = {
          proxyPass = "http://localhost:${toString config.services.headscale.port}";
          proxyWebsockets = true;
        };
      };
    };

    networking.firewall.interfaces."${config.services.tailscale.interfaceName}" = {
      allowedTCPPorts = [ 53 853 ];
      allowedUDPPorts = [ 53 853 ];
    };

    services.unbound = {
      inherit (config.homelab.cloud.vpn.client) enable;
      settings = {
        server = {
          interface = [
            config.services.tailscale.interfaceName
            "127.0.0.1"
          ];
          access-control = [ "100.64.0.0/10 allow" ];
          tls-cert-bundle = "/etc/ssl/certs/ca-certificates.crt";
          tls-upstream = "yes";
          so-reuseport = true;

          private-domain = lib.attrNames cfg.extraARecords;
          local-zone = 
               map (d: ''"${d}" redirect'') (lib.attrNames cfg.extraARecords)
            ++ cfg.extraZones;
          local-data = lib.mapAttrsToList (d: ip: ''"${d} IN A ${ip}"'') cfg.extraARecords
            ++ map (r: ''"${r}"'') cfg.extraData;
        };
        forward-zone = [
          {
            name = ".";
            forward-addr = [
              "176.9.93.198@853#dnsforge.de"
              "176.9.1.117@853#dnsforge.de"
              # "2a01:4f8:151:34aa::198@853#dnsforge.de"
              # "2a01:4f8:141:316d::117@853#dnsforge.de"
            ];
            forward-tls-upstream = "yes";
          }
        ];
      };
    };

  };
}
