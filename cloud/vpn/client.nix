{ config, lib, ... }:

let
  cfg = config.homelab.cloud.vpn.client;
  inherit (lib) mkEnableOption mkIf;
in
{

  options.homelab.cloud.vpn.client.enable = mkEnableOption "Tailscale client" // { default = true; };

  config = mkIf cfg.enable {
    homelab.ephemeral.persist.dirs = [ "/var/lib/tailscale" ];

    services.tailscale.enable = true;

    networking.firewall = {
      checkReversePath = "loose";
      allowedUDPPorts = [ config.services.tailscale.port ];
      interfaces."${config.services.tailscale.interfaceName}".allowedTCPPorts = [ 22 ];
    };

    # services.openssh.openFirewall = false;
  };
}
