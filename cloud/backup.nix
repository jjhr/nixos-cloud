{ config, lib, pkgs, ... }:

let
  service = "backup";
  cfg = config.homelab.cloud.${service};
  inherit (lib) mkOption mkIf mkMerge mkForce types
    mapAttrs flip;
in
{

  options.homelab.cloud.${service} = {

    authorizedPullKeys = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };

    snapshot = mkOption {
      description = "ZFS volumes to snapshot / prune";
      type = types.attrsOf (types.submodule {
        options = {
          snapshot = mkOption { type = types.bool; default = true; };
          extraConfig = mkOption {
            type = types.attrs;
            default = { };
          };
        };
      });
      default = { };
    };

    sync = mkOption {
      description = "ZFS volumes to sync";
      type = types.attrsOf types.str;
      default = { };
    };

    borg = mkOption {
      description = lib.mdDoc ''
        Borg jobs.

        **Setup:**

        1. Create SSH Key for borg to use: `ssh-keygen -N "" -t ed25519 -f ssh`
        2. Create Repo Key for borg: `ssh-keygen -N "" -t ed25519 -f repo`
        3. Add to secrets:
        ```
          "cloud/backup/sync/ssh": SSH private key
          "cloud/backup/sync/ssh.pub": SSH public key
          "cloud/backup/borg": Borg private key
        ```
        4. SSH to ''${config.homelab.hostName} and copy the pubkey to the remote server:
          `doas ssh-copy-id -i /run/secrets/cloud/backup/sync/ssh SSH_REMOTE`
        5. Create backup directory in SSH_REMOTE
      '';
      type = types.attrsOf (types.submodule {
        options = {
          repo = mkOption { type = types.str; };
          path = mkOption { type = types.str; };
          startAt = mkOption { type = with types; either str (listOf str); };
          extraConfig = mkOption {
            type = types.attrs;
            default = { };
          };
        };
      });
      default = { };
    };

  };

  config = mkMerge [

    (mkIf (cfg.sync != { } || cfg.authorizedPullKeys != [ ]) {
      users = {
        users.backup = {
          group = "backup";
          isSystemUser = true;
          useDefaultShell = true;
          createHome = true;
          home = "/var/lib/syncoid"; # syncoid refuses ssh connection if not in this folder
          extraGroups = mkForce [ ];
          openssh.authorizedKeys.keys = cfg.authorizedPullKeys;
        };
        groups.backup = { };
      };
      homelab.ephemeral.persist.dirs = [
        { directory = "/var/lib/syncoid"; user = "backup"; group = "backup"; }
      ];
    })

    (mkIf (cfg.sync != { }) {
      sops.secrets = {
        "cloud/backup/sync/ssh".owner = config.users.users.backup.name;
      };

      services.syncoid = {
        enable = cfg.sync != { };
        user = "backup";
        group = "backup";
        sshKey = config.sops.secrets."cloud/backup/sync/ssh".path;
        interval = "*-*-* 18:30:00";
        localTargetAllow = [ "compression" "mountpoint" "create" "mount" "receive" "rollback" "destroy" "change-key" ];
        commands = mapAttrs
          (_: target: {
            inherit target;
            extraArgs = [
              "--no-sync-snap" # sync only existing snapshots
              # "--debug"
              # "--force-delete"
              # "--no-rollback"
            ];
          })
          cfg.sync;
      };
    })

    (mkIf (cfg.snapshot != { }) {
      environment.systemPackages = [ pkgs.mbuffer pkgs.lzop ];
      services.sanoid = {
        enable = cfg.snapshot != { };
        #TODO move to options
        datasets = mapAttrs
          (_: { snapshot, extraConfig }: {
            autoprune = true;
            autosnap = snapshot;
            hourly = 2;
            daily = 2;
            monthly = 2;
            yearly = 0;
          } // extraConfig)
          cfg.snapshot;
      };
    })

    (mkIf (cfg.borg != { }) {
      sops.secrets = {
        "cloud/backup/sync/ssh" = { };
        "cloud/backup/sync/ssh.pub" = { };
        "cloud/backup/borg" = { };
      };
      homelab.ephemeral.persist.dirs = [ "/root/.ssh" ];

      services.borgbackup.jobs = flip mapAttrs cfg.borg
        (_: { repo, path, startAt, extraConfig }: {

          inherit repo startAt;
          environment.BORG_RSH = "ssh -i ${config.sops.secrets."cloud/backup/sync/ssh".path}";

          paths = [ path ];
          compression = "auto,lzma";

          encryption = {
            mode = "repokey-blake2";
            passCommand = "cat ${config.sops.secrets."cloud/backup/borg".path}";
          };

          prune.keep = {
            daily = 10;
            weekly = 4;
            monthly = 4;
            yearly = 3;
          };
        } // extraConfig);
    })

  ];
}
