{ config, lib, options, ... }:

let
  service = "mailer";
  inherit (lib) mkOption mkIf types mkEnableOption;
  cfg = config.homelab.cloud."${service}";
in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service;
    user = mkOption { type = types.str; example = "service@test.com"; };
    host = mkOption { type = types.str; example = "mail.test.com"; };
    port = mkOption { type = types.port; default = 587; };

    passFile = mkOption {
      internal = true;
      default = config.sops.secrets."cloud/mailer/pass".path;
    };
  };

  config = mkIf cfg.enable {
    sops.secrets."cloud/mailer/pass" = { group = config.users.groups.keys.name; mode = "0440"; };

    # programs.msmtp = {
    #   enable = true;
    #   setSendmail = true;
    #   accounts.default = {
    #     auth = true;
    #     inherit (cfg) host port user;
    #     from = cfg.user;
    #     passwordeval = "cat ${config.sops.secrets."cloud/mailer/pass".path}; echo";
    #     tls = true;
    #   };
    # };
  };

}
