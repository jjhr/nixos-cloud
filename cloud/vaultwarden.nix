{ config, lib, options, pkgs, ... }:

let
  service = "vaultwarden";
  cfg = config.homelab.cloud.${service};
  port = 23894;

  inherit (lib) mkOption mkEnableOption mkIf mkMerge types mdDoc
    optionalAttrs optionalString;
  inherit (config.homelab.cloud) mailer;
in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service // {
      description = mdDoc ''
        Whether to enable ${service}.

        Required secrets:
        - `cloud/bitwarden/admin_token`: Can be generated with `openssl rand -base64 48`
        - `cloud/bitwarden/ldap`: If `config.homelab.cloud.ldap.enable = true`.
          Periodically sends invitations to all LDAP users in group `users`.
          Password of user `bitwarden` in LDAP. Can be generated with `openssl
          rand -hex 48`.
      '';
    };
    domain = mkOption { type = types.str; example = "vault.test.com"; };
    extraConfig = mkOption {
      type = types.attrs;
      description = mdDoc "Overrides to services.vaultwarden.config.";
      default = { };
    };
    inherit (options.homelab.cloud) ldap;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      homelab.ephemeral.backup.dirs = [{
        directory = "/var/lib/bitwarden_rs";
        user = "vaultwarden";
        group = "vaultwarden";
      }];
      sops.secrets."cloud/bitwarden/admin_token" = {
        owner = "vaultwarden";
        mode = "0440";
      };

      services.nginx.enable = true;
      services.nginx.virtualHosts."${cfg.domain}" =
        config.homelab.lib.acmeForDomain cfg.domain
        // {
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://localhost:${toString port}";
            proxyWebsockets = true;
          };
        };

      services.vaultwarden = {
        enable = true;
        environmentFile = "/var/lib/bitwarden_rs/.env";
        config = {
          domain = "https://${cfg.domain}";
          signupsAllowed = false;
          rocketPort = port;
          websocketEnabled = true;
        }
        // optionalAttrs mailer.enable {
          smtpHost = mailer.host;
          smtpFrom = mailer.user;
          smtpUsername = mailer.user;
          smtpPort = mailer.port;
          # useSendmail = true;
          # sendmailCommand = "/run/wrappers/bin/sendmail";
        }
        // cfg.extraConfig;
      };

      systemd.services.vaultwarden_setup = {
        description = "Setup vaultwarden env file";
        before = [ "vaultwarden.service" ];
        wantedBy = [ "vaultwarden.service" ];
        script = ''
          cat << EOF > /var/lib/bitwarden_rs/.env
            ADMIN_TOKEN=$(cat ${config.sops.secrets."cloud/bitwarden/admin_token".path})
            ${optionalString mailer.enable "SMTP_PASSWORD=$(cat ${mailer.passFile})"}
          EOF
        '';
        serviceConfig = {
          Type = "oneshot";
          User = "vaultwarden";
          RemainAfterExit = "yes";
        };
      };

      users.users.vaultwarden.extraGroups = [ config.users.groups.keys.name ];
    }

    (
      let
        inherit (cfg) ldap;
        ldapConfigFile = (pkgs.formats.toml { }).generate "config.toml" {
          vaultwarden_url = "https://${cfg.domain}";
          vaultwarden_admin_token = "@ADMIN_TOKEN@";
          ldap_host = ldap.domain;
          ldap_port = 6360;
          ldap_ssl = true;
          ldap_bind_dn = "uid=bitwarden,ou=people,${ldap.baseDn}";
          ldap_bind_password = "@LDAP_PASSWORD@";
          ldap_search_base_dn = "ou=people,${ldap.baseDn}";
          ldap_search_filter = "(&(objectClass=person)(memberOf=cn=users,ou=groups,${ldap.baseDn}))";
          ldap_sync_interval_seconds = 60;
        };
      in
      mkIf ldap.enable {

        sops.secrets."cloud/bitwarden/ldap_pass".owner = "vaultwarden_ldap";

        systemd.services.vaultwarden_ldap = {
          wantedBy = [ "multi-user.target" ];

          preStart = ''
            sed \
              -e "s=@LDAP_PASSWORD@=$(<${config.sops.secrets."cloud/bitwarden/ldap_pass".path})=" \
              -e "s=@ADMIN_TOKEN@=$(<${config.sops.secrets."cloud/bitwarden/admin_token".path})=" \
              ${ldapConfigFile} \
              > /run/vaultwarden_ldap/config.toml
          '';

          serviceConfig = {
            Restart = "on-failure";
            RestartSec = "2s";
            ExecStart = "${pkgs.nur.repos.mic92.vaultwarden_ldap}/bin/vaultwarden_ldap";
            Environment = "CONFIG_PATH=/run/vaultwarden_ldap/config.toml";

            RuntimeDirectory = [ "vaultwarden_ldap" ];
            User = "vaultwarden_ldap";
          };
        };

        users.users.vaultwarden_ldap = {
          isSystemUser = true;
          group = "vaultwarden_ldap";
          extraGroups = [ "vaultwarden" ]; # for ADMIN_TOKEN
        };

        users.groups.vaultwarden_ldap = { };
      }
    )
  ]);
}
