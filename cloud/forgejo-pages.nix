{ config, lib, pkgs, ... }:

let
  service = "forgejo-pages";
  cfg = config.homelab.cloud.${service};
  port = 4430;

  inherit (lib) mkOption mkEnableOption types mkPackageOptionMD;
  # inherit (config.homelab.cloud) mailer;

in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service // {
      description = ''
        Whether to enable ${service}.

        Required secrets:
        - `cloud/forgejo_pages`: See `config.homelab.cloud.${service}.wildcardDnsProvider`. 
        Also this file can also include a line with `GITEA_API_TOKEN=...` for
        access to private repos.
      '';
    };
    package = mkPackageOptionMD pkgs "codeberg-pages" { } // {
      # Use latest release until maybe nixpkgs-24.05?
      default = pkgs.callPackage "${pkgs.path}/pkgs/development/tools/continuous-integration/codeberg-pages" {
        buildGoModule = args: pkgs.buildGoModule (args // rec {
          version = "4.6.3";
          src = pkgs.fetchFromGitea {
            domain = "codeberg.org";
            owner = "Codeberg";
            repo = "pages-server";
            rev = "v${version}";
            sha256 = "sha256-DRMRkRYOp74un+tXkkrrBqom5oqu8tSaMO7bN7utHb0=";
          };
          patches = [ ];
          postPatch = ''
            rm server/handler/handler_test.go
          '';
          vendorHash = "sha256-R/LuZkA2xHmu7SO3BVyK1C6n9U+pYn50kNkyLltn2ng=";
        });
      };
    };
    domain = mkOption { type = types.str; example = "pages.test.com"; };
    forgejoDomain = mkOption { type = types.str; example = "code.test.com"; };
    wildcardDnsProvider = mkOption {
      type = types.str;
      example = "hetzner";
      description = ''
        Code of the ACME DNS provider for the main domain wildcard.
        See https://go-acme.github.io/lego/dns/ for available values & additional environment variables.
        Environment Variables for lego must be added as secret `cloud/forgejo_pages`.
      '';
    };
  };

  config = lib.mkIf cfg.enable {

    sops.secrets."cloud/forgejo_pages" = { };
    homelab.ephemeral.persist.dirs = [ "/var/lib/private/${service}" ];

    networking.firewall.interfaces."${config.services.tailscale.interfaceName}".allowedTCPPorts = [ port ];

    systemd.services.${service} = {
      description = "Codeberg Pages";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      path = [ cfg.package ];

      # In older versions the secret naming for JWT was kind of confusing.
      # The file jwt_secret hold the value for LFS_JWT_SECRET and JWT_SECRET
      # wasn't persistent at all.
      # To fix that, there is now the file oauth2_jwt_secret containing the
      # values for JWT_SECRET and the file jwt_secret gets renamed to
      # lfs_jwt_secret.
      # We have to consider this to stay compatible with older installations.
      # preStart =

      serviceConfig = {
        Type = "simple";
        DynamicUser = true;
        User = service;
        EnvironmentFile = config.sops.secrets."cloud/forgejo_pages".path;
        ExecStart = "${cfg.package}/bin/pages";
        Restart = "always";
        WorkingDirectory = "/var/lib/${service}";
        RuntimeDirectory = service;
        StateDirectory = service;
        CapabilityBoundingSet = "";
        LockPersonality = true;
        PrivateDevices = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RestrictAddressFamilies = [ "AF_UNIX" "AF_INET" "AF_INET6" ];
        RestrictNamespaces = true;
        RestrictRealtime = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = [ "@system-service" "~@setuid @keyring" ];
        UMask = "0066";
      };

      environment = {
        PORT = toString port;
        PAGES_DOMAIN = cfg.domain;
        RAW_DOMAIN = "raw.${cfg.domain}";
        GITEA_ROOT = "https://${cfg.forgejoDomain}";
        # RAW_INFO_PAGE
        # ACME_API = "https://acme.mock.director";
        ACME_EMAIL = config.homelab.cloud.nginx.acmeEmail;
        ACME_ACCEPT_TERMS = "true";
        # ACME_USE_RATE_LIMITS
        DNS_PROVIDER = cfg.wildcardDnsProvider;
      };
    };

    # users.users.${service} = {
    #   home = stateDir;
    #   useDefaultShell = true;
    #   group = service;
    #   isSystemUser = true;
    # };
  };
}
