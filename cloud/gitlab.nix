{ config, lib, options, ... }:

let
  service = "gitlab";
  cfg = config.homelab.cloud.${service};
  pagesPort = 48414;

  inherit (lib) mkOption mkEnableOption types
    optionalAttrs hasAttr;
  inherit (config.homelab.cloud) mailer;
in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service;
    domain = mkOption { type = types.str; example = "git.test.com"; };
    pages.domain = mkOption { type = types.str; example = "pages.test.com"; };
    inherit (options.homelab.cloud) ldap; 
  };

  config = lib.mkIf cfg.enable {

    assertions = [{
      assertion = hasAttr cfg.pages.domain config.homelab.cloud.nginx.wildcardDomains;
      message = "You need a wildcard certificate for *.${cfg.pages.domain}";
    }];

    homelab.ephemeral.persist.dirs =
      [{ directory = config.services.gitlab.statePath; user = "gitlab"; group = "gitlab"; }];
    homelab.ephemeral.backup.dirs =
      [{ directory = config.services.gitlab.backup.path; user = "gitlab"; group = "gitlab"; }];

    sops.secrets = {
      # All keys min 32 chars
      "cloud/gitlab/database".owner = "gitlab";
      "cloud/gitlab/root".owner = "gitlab";
      "cloud/gitlab/secret".owner = "gitlab";
      "cloud/gitlab/otp".owner = "gitlab";
      "cloud/gitlab/db".owner = "gitlab";
      # openssl rand -base64 32
      "cloud/gitlab/pages".owner = "gitlab";
      # openssl genrsa 2048
      "cloud/gitlab/jws".owner = "gitlab";
    };

    services.nginx.enable = true;
    services.nginx.virtualHosts."${cfg.domain}" =
      config.homelab.lib.acmeForDomain cfg.domain
      // {
        forceSSL = true;
        locations."/".proxyPass = "http://unix:/run/gitlab/gitlab-workhorse.socket";
        locations."/".extraConfig = "client_max_body_size 4G;";
      };
    services.nginx.virtualHosts."*.${cfg.pages.domain}" =
      config.homelab.lib.acmeForDomain cfg.pages.domain
      // {
        addSSL = true;
        locations."/".proxyPass = "http://localhost:${toString pagesPort}";
      };

    users.users.gitlab.extraGroups = [ config.users.groups.keys.name ];

    services.gitlab = {
      enable = true;
      port = 443;
      https = true;
      host = cfg.domain;

      statePath = "/var/lib/gitlab/state";
      backup.path = "/var/lib/gitlab/backup";

      databasePasswordFile = config.sops.secrets."cloud/gitlab/database".path;
      initialRootPasswordFile = config.sops.secrets."cloud/gitlab/root".path;
      secrets = {
        secretFile = config.sops.secrets."cloud/gitlab/secret".path;
        otpFile = config.sops.secrets."cloud/gitlab/otp".path;
        dbFile = config.sops.secrets."cloud/gitlab/db".path;
        jwsFile = config.sops.secrets."cloud/gitlab/jws".path;
      };

      smtp = {
        inherit (mailer) port enable;
        tls = true;
        domain = mailer.host;
        address = mailer.host;
        username = mailer.user;
        passwordFile = mailer.passFile;
      };


      extraConfig = {
        gitlab = {
          email_from = mailer.user;
          email_display_name = "GitLab";
          # email_reply_to = "noreply-${mailer.user}";
        };

        pages = {
          enabled = true;
          settings = {
            pages-domain = cfg.pages.domain;
            internal-gitlab-server = "https://${cfg.domain}";
            listen-http = "127.0.0.1:${toString pagesPort}";

            external_http = [ "127.0.0.1:8090" ];
            access_control = false;
            # port = 443;
            https = true;
            artifacts_server = false;
          };
        };

        ldap = optionalAttrs (cfg.ldap != null) {
          enabled = true;
          servers.main = {
            label = "LDAP";
            host = cfg.ldap.domain;
            port = 6360;

            uid = "uid";
            encryption = "simple_tls";
            bind_dn = "uid=gitlab,ou=people,${cfg.ldap.baseDn}";
            password._secret = config.sops.secrets."cloud/gitlab/root".path;
            admin_group = "lldap_admin";

            active_directory = false;

            allow_username_or_email_login = true;


            base = "ou=people,${cfg.ldap.baseDn}";
            user_filter = "(objectClass=person)";
            group_base = "ou=groups,${cfg.ldap.baseDn}";

            attributes = {
              username = [ "uid" ];
              email = [ "mail" ];
              name = "cn";
              first_name = "givenName";
              last_name = "sn";
            };
          };
        };
      };
    };
  };
}
