{ config, lib, pkgs, ... }:

let
  service = "gitlab-runner";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkOption mkIf mkForce mdDoc types
    length concatStringsSep pipe nameValuePair listToAttrs;
in
{

  options.homelab.cloud.${service}.runners = mkOption {
    type = types.listOf (types.enum [ "nix-container" "nix-shell" "default" ]);
    default = [ ];
    description = mdDoc ''
      Run a list of gitlab runners on this machine.
      - nix-container = nix shell executor inside alpine container (tag: nix)
      - nix-shell = nix shell executor (tag: nix)
      - default = everything else
    '';
  };

  config = mkIf (length cfg.runners != 0) {

    assertions = [{
      assertion = config.homelab.containers.enable == "docker";
      message = "Gitlab-runner currently only works with docker.";
    }];

    homelab.ephemeral.persist.dirs = [ "/var/lib/gitlab-runner" ];

    # File should contain at least these two variables:
    # `CI_SERVER_URL`
    # `REGISTRATION_TOKEN`
    sops.secrets."cloud/gitlab-runner/registration".owner = "gitlab-runner";

    users = {
      groups.gitlab-runner = { };
      users.gitlab-runner = {
        isSystemUser = true;
        group = "gitlab-runner";
      };
    };

    # Enable networking inside docker/podman builds
    boot.kernel.sysctl."net.ipv4.ip_forward" = true; # 1

    systemd.services.gitlab-runner.serviceConfig = {
      DynamicUser = mkForce false;
      User = "gitlab-runner";
      Group = "gitlab-runner";
    };

    services.gitlab-runner = {
      enable = true;
      settings.concurrent = 4;
      services =
        let
          runners = {
            nix-container = {
              registrationConfigFile = config.sops.secrets."cloud/gitlab-runner/registration".path;
              dockerImage = "alpine";
              dockerVolumes = [
                "/nix/store:/nix/store:ro"
                "/persist/opt/Xilinx:/opt/Xilinx:ro"
                "/nix/var/nix/db:/nix/var/nix/db:ro"
                "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
              ];
              dockerDisableCache = true;
              dockerPrivileged = true;
              preBuildScript = pkgs.writeScript "setup-container" ''
                mkdir -p -m 0755 /nix/var/log/nix/drvs
                mkdir -p -m 0755 /nix/var/nix/gcroots
                mkdir -p -m 0755 /nix/var/nix/profiles
                mkdir -p -m 0755 /nix/var/nix/temproots
                mkdir -p -m 0755 /nix/var/nix/userpool
                mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
                mkdir -p -m 1777 /nix/var/nix/profiles/per-user
                mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
                mkdir -p -m 0700 "$HOME/.nix-defexpr"
                . ${pkgs.nix}/etc/profile.d/nix.sh
                ${pkgs.nix}/bin/nix-env -i ${concatStringsSep " " (with pkgs; [ nix cacert git openssh cachix ])}
                ${pkgs.nix}/bin/nix-channel --add https://nixos.org/channels/nixpkgs-unstable
                ${pkgs.nix}/bin/nix-channel --update nixpkgs

                mkdir -p /etc/nix
                echo 'experimental-features = nix-command flakes' > /etc/nix/nix.conf
              '';
              environmentVariables = {
                ENV = "/etc/profile";
                USER = "root";
                NIX_REMOTE = "daemon";
                PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
                NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
              };
              tagList = [ "nix" ];
            };

            nix-shell = {
              registrationConfigFile = config.sops.secrets."cloud/gitlab-runner/registration".path;
              executor = "shell";
              tagList = [ "nix" ];
            };

            default = {
              dockerImage = "debian";
              registrationConfigFile = config.sops.secrets."cloud/gitlab-runner/registration".path;
              dockerAllowedImages = [ "*" ];
            };
          };
        in
        pipe cfg.runners [
          (map (name: nameValuePair name runners."${name}"))
          listToAttrs
        ];
    };
  };

}
