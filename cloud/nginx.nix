{ config, lib, ... }:

let
  service = "nginx";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkOption mkEnableOption mkIf mkMerge types;
in
{

  options.homelab.cloud.${service} = {

    enable = mkEnableOption "Nginx reverse proxy with Lets Encrypt wildcard cert";

    openFirewall = mkOption {
      type = types.enum [ "tailnet" "external" ];
      description = "Whether to open firewall ports to the internet / only in tailnet.";
      default = "tailnet";
    };

    acmeEmail = mkOption { type = types.str; example = "postmaster@test.com"; };

    wildcardDomains = mkOption {
      description = ''
        Domains to aquire wildcard certs for.
        Also adds sops secret for DNS provider (cloud/certs/<domain>).
      '';
      type = types.attrsOf (types.submodule {
        options = {
          provider = mkOption { type = types.str; };
          dnsPropagationCheck = mkOption { type = types.bool; default = true; };
          dnsResolver = mkOption { type = types.nullOr types.str; default = "1.1.1.1:53"; };
          extraDomainNames = mkOption { type = types.listOf types.str; default = [ ]; };
        };
      });
      default = { };
      example = ''{ "test.com" = { provider = "hetzner"; }; }'';
    };

    tunnels = mkOption {
      type = types.attrsOf types.str;
      example = ''{
        "cloud.host.de" = "cloud.internal.host.de";
      }'';
      default = { };
    };

  };

  config = lib.mkIf cfg.enable {

    homelab.ephemeral.persist.dirs = [
      { directory = "/var/lib/acme"; user = "acme"; group = "acme"; }
    ];
    users.users.nginx.extraGroups = [ "acme" ];

    sops.secrets = lib.mapAttrs'
      (domain: _:
        lib.nameValuePair "cloud/certs/${domain}" { owner = "acme"; })
      cfg.wildcardDomains;

    networking.firewall =
      if cfg.openFirewall == "external" then
        { allowedTCPPorts = [ 80 443 ]; }
      else
        { interfaces."${config.services.tailscale.interfaceName}".allowedTCPPorts = [ 80 443 ]; };

    services.nginx = mkMerge [
      {
        enable = true;
        recommendedGzipSettings = true;
        recommendedOptimisation = true;
        recommendedProxySettings = true;
        recommendedTlsSettings = true;

        # Only allow PFS-enabled ciphers with AES256
        sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

        # Return 404 on unmatched vhost
        virtualHosts = {
          _ = {
            # addSSL = true;
            # enableACME = true;
            rejectSSL = true;
            default = true;
            locations."/".return = "404";
          };
        }
        # add https tunnels
        // (builtins.mapAttrs
          (from: to:
            config.homelab.lib.acmeForDomain from // {
              forceSSL = true;
              locations."/" = {
                proxyPass = "https://${to}";
                proxyWebsockets = true;
                recommendedProxySettings = false;
                extraConfig = ''
                  proxy_ssl_server_name on;
                  proxy_ssl_session_reuse on;

                  proxy_set_header Host ${to};
                  proxy_set_header X-Real-IP $remote_addr;
                  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                  proxy_set_header X-Forwarded-Proto $scheme;
                  proxy_set_header X-Forwarded-Host ${to};
                  proxy_set_header X-Forwarded-Server ${to};
                '';
              };
            })
          cfg.tunnels);
      }

      (mkIf config.services.unbound.enable {
        resolver.addresses = [ "127.0.0.1:53" "100.100.100.100:53" ];
        proxyResolveWhileRunning = true;
      })
    ];


    security.acme = {
      acceptTerms = true;
      defaults.email = cfg.acmeEmail;

      certs = lib.mapAttrs
        (domain: { provider, extraDomainNames, dnsPropagationCheck, dnsResolver }: {
          inherit domain dnsPropagationCheck dnsResolver;
          dnsProvider = provider;
          extraDomainNames = [ "*.${domain}" ] ++ extraDomainNames;
          credentialsFile = config.sops.secrets."cloud/certs/${domain}".path;
        })
        cfg.wildcardDomains;
    };


  };
}
