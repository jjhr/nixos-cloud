{ config, lib, ... }:
let
  service = "metrics";
  cfg = config.homelab.cloud.${service};

  inherit (lib) mkEnableOption mkOption mkMerge mkIf types
    mapAttrsToList optionalAttrs pipe filterAttrs recursiveMerge flatten length attrNames;
  inherit (config.homelab.cloud) mailer;
in
{
  options.homelab.cloud.${service} = {
    consumer = {
      enable = mkEnableOption "Prometheus scrapers + Grafana monitoring";
      domain = mkOption { type = types.str; example = "stats.test.com"; };
      # grafanaSettings = options.services.grafana.settings;
    };
    producer.enable = mkEnableOption "Prometheus exporters";

    export = mkOption {
      description = "Centralized definition of exporters associated by hostname";
      type = types.attrsOf types.attrs;
      default = { };
    };
    scrape = mkOption {
      description = "Centralized definition of extra scraper options";
      type = types.attrs;
      default = { };
    };
  };

  config =
    let

      consumer = {
        homelab.ephemeral.backup.dirs = [
          { directory = "/var/lib/grafana"; user = "grafana"; group = "grafana"; }
          { directory = "/var/lib/prometheus2"; user = "prometheus"; group = "prometheus"; }
        ];

        services.nginx.enable = true;
        services.nginx.virtualHosts."${cfg.consumer.domain}" =
          config.homelab.lib.acmeForDomain cfg.consumer.domain
          // {
            addSSL = true;
            locations."/" = {
              proxyPass = "http://localhost:${toString config.services.grafana.settings.server.http_port}";
              proxyWebsockets = true;
            };
          };

        services.grafana = {
          enable = true;
          settings = {
            analytics.reporting_enabled = false;
            server = {
              inherit (cfg.consumer) domain;
              http_port = 30103;
            };
            smtp = optionalAttrs mailer.enable {
              enable = true;
              inherit (mailer) host user;
              password = "$__file{${mailer.passFile}}";
              from_address = mailer.user;
            };
          };
          provision = {
            enable = true;
            datasources.settings.datasources = [
              {
                name = "Prometheus";
                type = "prometheus";
                access = "proxy";
                url = "http://127.0.0.1:${toString config.services.prometheus.port}";
              }
            ];
          };
        };
        systemd.services.grafana.serviceConfig.SupplementaryGroups = [ config.users.groups.keys.name ];

        services.prometheus = {
          enable = true;
          port = 16468;
          listenAddress = "127.0.0.1";

          scrapeConfigs =
            let
              hostsByExporter = pipe cfg.export [
                (mapAttrsToList (host: exporters:
                  pipe exporters [
                    (filterAttrs (_: { enable, ... }: enable))
                    (mapAttrsToList (name: exporter: { "${name}"."${host}" = exporter; }))
                  ]
                ))
                flatten
                recursiveMerge
                (filterAttrs (_: hosts: length (attrNames hosts) > 0))
              ];
              exporterToScraper = {
                node = hosts: {
                  job_name = "nodes";
                  static_configs = mapAttrsToList
                    (host: { port, ... }: {
                      targets = [ "${config.homelab.hosts."${host}"}:${toString port}" ];
                      labels.host = host;
                    })
                    hosts;
                };
                blackbox = _hosts: {
                  job_name = "uptimes";
                  metrics_path = "/probe";
                  params.module = [ "http_2xx" ];
                  static_configs = [{
                    inherit (cfg.scrape.blackbox) targets;
                  }];
                  relabel_configs = [
                    {
                      source_labels = [ "__address__" ];
                      target_label = "__param_target";
                    }
                    {
                      source_labels = [ "__param_target" ];
                      target_label = "instance";
                    }
                    {
                      target_label = "__address__";
                      replacement = "127.0.0.1:${toString config.services.prometheus.exporters.blackbox.port}";
                    }
                  ];
                };
              };
            in
            mapAttrsToList (name: exporterToScraper."${name}") hostsByExporter;
        };
      };

      producer = {
        services.prometheus.exporters = cfg.export."${config.homelab.hostName}";
        networking.firewall.interfaces."${config.services.tailscale.interfaceName}".allowedTCPPorts =
          if !cfg.consumer.enable
          then
            mapAttrsToList
              (_: { port, ... }: port)
              cfg.export."${config.homelab.hostName}"
          else [ ];
      };
    in
    mkMerge [
      (mkIf cfg.consumer.enable consumer)
      (mkIf cfg.producer.enable producer)
    ];
}
