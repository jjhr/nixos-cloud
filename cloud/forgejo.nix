{ config, lib, options, ... }:

let
  service = "forgejo";
  cfg = config.homelab.cloud.${service};
  port = 45723;

  inherit (lib) mkOption mkEnableOption types mdDoc;
  inherit (config.homelab.cloud) mailer;
in
{

  options.homelab.cloud.${service} = {
    enable = mkEnableOption service;
    domain = mkOption { type = types.str; example = "git.test.com"; };
    banner = mkOption {
      type = types.str;
      example = "My cool forge";
      default = "homelab/forge";
      description = "Text for banner on forgejo's homepage.";
    };
    firstStart = mkOption {
      type = types.bool;
      description = mdDoc ''
        If gitea/forgejo is started for the first time. One needs to create an user
        manually (which becomes admin) and configure LDAP. After this, one can
        set `firstStart` to `false`, which also disables registration. 

        See also <https://github.com/lldap/lldap/blob/main/example_configs/gitea.md>.
      '';
    };
  };

  config = lib.mkIf cfg.enable {

    homelab.ephemeral.backup.dirs = [
      { directory = "${config.services.forgejo.stateDir}/data"; user = "forgejo"; group = "forgejo"; }
      { directory = config.services.forgejo.repositoryRoot; user = "forgejo"; group = "forgejo"; }
      { directory = config.services.forgejo.dump.backupDir; user = "forgejo"; group = "forgejo"; }
    ];
    homelab.ephemeral.persist.dirs = [
      { directory = "${config.services.forgejo.customDir}/conf"; user = "forgejo"; group = "forgejo"; }
    ];

    services.nginx.enable = true;
    services.nginx.virtualHosts."${cfg.domain}" =
      config.homelab.lib.acmeForDomain cfg.domain
      // {
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://localhost:${toString port}";
          proxyWebsockets = true;
        };
      };


    systemd.services.forgejo = {
      serviceConfig.SupplementaryGroups = [ config.users.groups.keys.name ];
      unitConfig.RequiresMountsFor = [
        "${config.services.forgejo.stateDir}/data"
        config.services.forgejo.repositoryRoot
        config.services.forgejo.dump.backupDir
        "${config.services.forgejo.customDir}/conf"
      ];
    };


    services.forgejo = {
      enable = true;
      # lfs.enable = true;

      database.type = "sqlite3";
      mailerPasswordFile = mailer.passFile;

      settings = {
        DEFAULT.APP_NAME = cfg.banner;
        server = {
          DOMAIN = cfg.domain;
          ROOT_URL = "https://${cfg.domain}/";
          DISABLE_ROUTER_LOG = true;
          HTTP_PORT = port;
          HTTP_ADDRESS = "127.0.0.1";
          # SSH_PORT = 2222;
        };
        mailer = {
          ENABLED = true;
          FROM = mailer.user;
          USER = mailer.user;
          SMTP_ADDR = "${mailer.host}:${toString mailer.port}";
        };

        session = {
          COOKIE_SECURE = true;
          SAME_SITE = "strict";
        };

        service.DISABLE_REGISTRATION = !cfg.firstStart;

        actions.ENABLED = true;
        # metrics.ENABLED = true;
        log.LEVEL = "Warn";
      };
    };
  };
}
